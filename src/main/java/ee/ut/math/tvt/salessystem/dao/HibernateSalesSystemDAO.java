package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.PurchaseHistory;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;


    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();

        em.getTransaction().begin();
        em.persist(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        em.persist(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        em.persist(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        em.persist(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        em.getTransaction().commit();

    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<StockItem> findStockItems() {
        Query q = em.createQuery("SELECT e FROM StockItem e");
        return q.getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        return em.find(StockItem.class, id);
    }

    @Override
    public List<Purchase> findPurchases() {
        return em.createQuery("SELECT e FROM Purchase e").getResultList();
    }

    @Override
    public Purchase findPurchase(LocalDateTime dateTime) {
        return em.find(Purchase.class, dateTime);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Purchase> lastTenPurchases(){
        Query q = em.createQuery("SELECT e FROM Purchase e order by e.dateTime desc").setMaxResults(10);
        return q.getResultList();
    }
    @Override
    public void savePurchase(Purchase purchase) {
        em.persist(purchase);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        em.persist(stockItem);
    }


    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }
}