package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PurchaseHistory {
    private final SalesSystemDAO dao;

    private static final Logger log = LogManager.getLogger(PurchaseHistory.class);

    public PurchaseHistory(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public List<Purchase> getAll() {
        List<Purchase> found = dao.findPurchases();
        log.debug("purchases found: " + found);
        return found;
    }

    public List<Purchase> getLastTen() {
        List<Purchase> found = dao.lastTenPurchases();
        //List<Purchase> found = dao.findPurchases().subList(Math.max(dao.findPurchases().size() - 10, 0), dao.findPurchases().size());
        log.debug("purchases found: " + found);
        return found;
    }

    public List<Purchase> getBetweenDates(LocalDate start, LocalDate end){
        List<Purchase> found = new ArrayList<>();
        for (Purchase purchase : dao.findPurchases()) {
            if ((purchase.getDate().isAfter(start) || purchase.getDate().isEqual(start))
                    && (purchase.getDate().isBefore(end) || purchase.getDate().isEqual(end)))
                found.add(purchase);
        }
        log.debug("purchases found: " + found);
        return found;
    }
}
