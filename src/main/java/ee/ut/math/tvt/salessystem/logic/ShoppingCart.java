package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ShoppingCart {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);


    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        StockItem stockItem = item.getStockItem();
        int quantity = item.getQuantity();
        log.info("adding: " + item +" qty " + quantity);

        if(quantity > stockItem.getQuantity()){
            throw new SalesSystemException("Quantity exceeds stock, current stock: "+stockItem.getQuantity());
        }
        else if(quantity <1){
            throw new SalesSystemException("Quantity must be above 0");
        }
        // if solditem contains in shopping cart
        if (items.stream().anyMatch(o -> o.getId().equals(item.getId()))) {
            log.debug("item " + item + " already in shopping cart. increasing quantity.");
            // find matching solditem and set new quantity
            for (SoldItem soldItem : items) {
                if (soldItem.getId().equals(item.getId())) {
                    if (soldItem.getQuantity() + quantity > soldItem.getStockItem().getQuantity())
                        throw new SalesSystemException("Quantity exceeds stock, current stock: "+stockItem.getQuantity());
                    soldItem.setQuantity(soldItem.getQuantity() + quantity);
                }
            }
        }
        else {
            items.add(item);
            log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
        }
    }

    public List<SoldItem> getAll() {
        log.info("getting list of items");
        return items;
    }

    public void cancelCurrentPurchase() {
        log.info("clearing shopping cart");
        items.clear();
    }

    public void submitCurrentPurchase() {
        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        try {
            Purchase purchase = new Purchase(LocalDateTime.now(), new ArrayList<>(items));
            for (SoldItem item : items) {
                StockItem stockItem = item.getStockItem();
               stockItem.setQuantity(stockItem.getQuantity() - item.getQuantity());
            }
            dao.savePurchase(purchase);

            dao.commitTransaction();
            log.info("purchase complete");
            log.debug("total saved purchases: " + dao.findPurchases());
            items.clear();
        } catch (Exception e) {
            log.error(e.toString());
            dao.rollbackTransaction();
            throw e;
        }
    }
}
