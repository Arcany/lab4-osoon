package ee.ut.math.tvt.salessystem.logic;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {
    Properties props;

    public PropertiesReader(Properties props) {
        this.props = props;
    }

    public void read(){
        try (InputStream inputStream = ClassLoader.getSystemResourceAsStream("application.properties")) {
            props.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
