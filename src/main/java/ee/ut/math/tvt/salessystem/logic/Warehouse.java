package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Warehouse {
    private final SalesSystemDAO dao;
    private static final Logger log = LogManager.getLogger(Warehouse.class);


    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public void addStock(StockItem item){
        dao.beginTransaction();
        try {
            if (item.getName().equals(""))
                throw new SalesSystemException("Name required");
            if (item.getQuantity() < 0)
                throw new SalesSystemException("Negative quantity");
            StockItem stockItem = dao.findStockItem(item.getId());
            if (stockItem == null) {
                dao.saveStockItem(item);
                log.debug("Added new item: " + item + " to stock");
            }
            else {
                int quantity = item.getQuantity();
                stockItem.setQuantity(stockItem.getQuantity()+quantity);
                log.debug("Increased " + item + " quantity by " + quantity);
            }
            dao.commitTransaction();
        } catch (SalesSystemException e) {
            log.error(e.toString());
            throw e;
        }
    }
}
