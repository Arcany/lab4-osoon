package ee.ut.math.tvt.salessystem.dataobjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDateTime dateTime;
    private LocalDate date;
    private LocalTime time;
    private double total;
    @OneToMany(cascade = CascadeType.ALL)
    private List<SoldItem> soldItems;
    @Transient
    private static final Logger log = LogManager.getLogger(Purchase.class);

    public Purchase(LocalDateTime dateTime, List<SoldItem> soldItems) {
        this.dateTime = dateTime;
        this.soldItems = soldItems;
        this.date = dateTime.toLocalDate();
        this.time = dateTime.toLocalTime();
        this.total = soldItems.stream().mapToDouble(SoldItem::getSum).sum();
        log.debug("created purchase: " + this);
    }

    public Purchase() {

    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public LocalDate getDate() {
        return date;
    }

    public double getTotal() {
        return total;
    }

    public String getTime() {
        return time.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public List<SoldItem> getSoldItems() {
        return soldItems;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "dateTime=" + dateTime +
                ", total=" + total +
                '}';
    }
}
