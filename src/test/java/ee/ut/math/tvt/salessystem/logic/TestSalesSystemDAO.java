package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TestSalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<Purchase> purchaseList;

    public TestSalesSystemDAO() {
        this.stockItemList = new ArrayList<>();
        this.purchaseList = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public List<Purchase> findPurchases() {
        return purchaseList;
    }

    @Override
    public Purchase findPurchase(LocalDateTime dateTime) {
        for (Purchase purchase : purchaseList) {
            if (purchase.getDateTime() == dateTime)
                return purchase;
        }
        return null;
    }


    @Override
    public void savePurchase(Purchase purchase) {
        purchaseList.add(purchase);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }

    @Override
    public List<Purchase> lastTenPurchases() {
        return findPurchases().subList(Math.max(findPurchases().size() - 10, 0), findPurchases().size());
    }
}
