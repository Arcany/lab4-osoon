package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class WarehouseTests {

    private SalesSystemDAO daoMock;
    private SalesSystemDAO dao;
    private Warehouse warehouse;
    private Warehouse warehouseMock;
    private StockItem existingItem;
    private StockItem newItem;
    private StockItem negativeItem;

    private static final Logger log = LogManager.getLogger(WarehouseTests.class);

    @Before
    public void setUp() {
        daoMock = mock(InMemorySalesSystemDAO.class);
        dao = new InMemorySalesSystemDAO();
        warehouse = new Warehouse(dao);
        warehouseMock = new Warehouse(daoMock);
        existingItem = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5);
        newItem = new StockItem(7L, "Foo", "Bar", 11.0, 5);
        negativeItem = new StockItem(8L, "Foo", "Bar", 11.0, -5);
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        log.info("testing testAddingItemBeginsAndCommitsTransaction");
        InOrder order = inOrder(daoMock);
        warehouseMock.addStock(existingItem);
        order.verify(daoMock).beginTransaction();
        order.verify(daoMock).commitTransaction();
        verify(daoMock, times(1)).beginTransaction();
        verify(daoMock, times(1)).commitTransaction();
        log.info("testAddingItemBeginsAndCommitsTransaction successful");
    }

    @Test
    public void testAddingNewItem(){
        log.info("testing testAddingNewItem");

        int before = dao.findStockItems().size();
        warehouse.addStock(newItem);
        int after = dao.findStockItems().size();

        log.debug("comaring StockItem list sizes "+ before + " and " + after);
        assertNotEquals(before, after);

        log.info("testAddingNewItem successful");
    }

    @Test
    public void testAddingExistingItem(){
        log.info("testing testAddingExistingItem");

        int before = 0;
        for (StockItem stockItem : dao.findStockItems()) {
            before += stockItem.getQuantity();
        }
        warehouse.addStock(existingItem);
        int after = 0;
        for (StockItem stockItem : dao.findStockItems()) {
            after += stockItem.getQuantity();
        }
        log.debug("comparing Stockitem quantities: " + before +" and "+after);
        assertNotEquals(before, after);
        log.debug("assertNotEquals successful");

        verify(daoMock, never()).saveStockItem(existingItem);
        log.debug("saveStockItem() never called");

        log.info("testAddingExistingItem successful");
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantity(){
        log.info("testing testAddingItemWithNegativeQuantity");
        warehouse.addStock(negativeItem);
        log.info("testAddingItemWithNegativeQuantity successful");

    }


}