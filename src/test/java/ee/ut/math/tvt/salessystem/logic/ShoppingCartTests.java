package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ShoppingCartTests {

    private SalesSystemDAO daoMock;
    private ShoppingCart shoppingCartMock;
    private StockItem item;

    private static final Logger log = LogManager.getLogger(WarehouseTests.class);

    @Before
    public void setUp() {
        daoMock = mock(InMemorySalesSystemDAO.class);
        shoppingCartMock = new ShoppingCart(daoMock);
        item = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5);
    }

    // add item tests
    @Test
    public void testAddingExistingItem(){
        log.info("testing testAddingExistingItem");
        shoppingCartMock.addItem(new SoldItem(item, 1));
        int before = shoppingCartMock.getAll().size();
        shoppingCartMock.addItem(new SoldItem(item, 1));
        int after = shoppingCartMock.getAll().get(0).getQuantity();
        log.debug("comparing " + item + " quantities: " + before +" and "+after);
        assertTrue(after + " should be larger than " + before, after > before);
        log.info("testAddingExistingItem successful");
    }

    @Test
    public void testAddingNewItem(){
        log.info("testing testAddingNewItem");
        int before = shoppingCartMock.getAll().size();
        shoppingCartMock.addItem(new SoldItem(item, 1));
        int after = shoppingCartMock.getAll().size();
        log.debug("comparing shopping cart size: " + before +" and "+ after);
        assertTrue(after + " should be larger than " + before, after > before);
        log.info("testAddingNewItem successful");
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantity(){
        log.info("testing testAddingItemWithNegativeQuantity");
        shoppingCartMock.addItem(new SoldItem(item, -1));
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantityTooLarge(){
        int qty = item.getQuantity();
        log.info("testing testAddingItemWithQuantityTooLarge");
        shoppingCartMock.addItem(new SoldItem(item, qty+1));
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantitySumTooLarge(){
        int qty = item.getQuantity();
        log.info("testing testAddingItemWithQuantitySumTooLarge");
        shoppingCartMock.addItem(new SoldItem(item, qty));
        shoppingCartMock.addItem(new SoldItem(item, qty));
    }

    // submitCurrentPurchase tests

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity(){
        log.info("testing testSubmittingCurrentPurchaseDecreasesStockItemQuantity");
        Warehouse warehouseMock = new Warehouse(daoMock);
        StockItem item1 = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 1);
        StockItem item2 = new StockItem(7L, "Foo", "Bar", 11.0, 2);
        warehouseMock.addStock(item1);
        warehouseMock.addStock(item2);
        shoppingCartMock.addItem(new SoldItem(item1, 1));
        shoppingCartMock.addItem(new SoldItem(item2, 2));
        shoppingCartMock.submitCurrentPurchase();
        assertEquals("Stockitem should be 0", 0, item1.getQuantity());
        assertEquals("Stockitem should be 0", 0, item2.getQuantity());
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction(){
        log.info("testing testSubmittingCurrentPurchaseBeginsAndCommitsTransaction");
        InOrder order = inOrder(daoMock);
        shoppingCartMock.addItem(new SoldItem(item, 5));
        shoppingCartMock.submitCurrentPurchase();
        order.verify(daoMock).beginTransaction();
        order.verify(daoMock).commitTransaction();
        verify(daoMock, times(1)).beginTransaction();
        verify(daoMock, times(1)).commitTransaction();
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem(){
        log.info("testing testSubmittingCurrentOrderCreatesHistoryItem");
        TestSalesSystemDAO testDAO = new TestSalesSystemDAO();
        ShoppingCart testCart = new ShoppingCart(testDAO);
        StockItem item1 = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 1);
        StockItem item2 = new StockItem(7L, "Foo", "Bar", 11.0, 2);
        SoldItem soldItem1 = new SoldItem(item1, 1);
        SoldItem soldItem2 = new SoldItem(item2, 2);
        testCart.addItem(soldItem1);
        testCart.addItem(soldItem2);
        testCart.submitCurrentPurchase();
        assertEquals(1, testDAO.findPurchases().size());
        assertTrue(testDAO.findPurchases().get(0).getSoldItems().containsAll(Arrays.asList(soldItem1, soldItem2)));
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime(){
        log.info("testing testSubmittingCurrentOrderSavesCorrectTime");
        TestSalesSystemDAO testDAO = new TestSalesSystemDAO();
        ShoppingCart testCart = new ShoppingCart(testDAO);
        testCart.submitCurrentPurchase();
        LocalDateTime dateTime = LocalDateTime.now();
        Duration duration = Duration.between(testDAO.findPurchases().get(0).getDateTime(), dateTime);
        assertTrue(duration.getSeconds() < 1 );
    }

    @Test
    public void testCancellingOrder(){
        log.info("testing testCancellingOrder");
        TestSalesSystemDAO testDAO = new TestSalesSystemDAO();
        ShoppingCart testCart = new ShoppingCart(testDAO);
        StockItem item1 = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 1);
        StockItem item2 = new StockItem(7L, "Foo", "Bar", 11.0, 2);
        SoldItem soldItem1 = new SoldItem(item1, 1);
        SoldItem soldItem2 = new SoldItem(item2, 2);
        testCart.addItem(soldItem1);
        testCart.cancelCurrentPurchase();
        testCart.addItem(soldItem2);
        testCart.submitCurrentPurchase();
        assertEquals(testDAO.findPurchases().get(0).getSoldItems(), Collections.singletonList(soldItem2));
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged(){
        log.info("testing testCancellingOrderQuanititesUnchanged");
        ShoppingCart testCart = new ShoppingCart(daoMock);
        int qty = item.getQuantity();
        testCart.addItem(new SoldItem(item, 5));
        testCart.cancelCurrentPurchase();
        assertEquals(qty, item.getQuantity());

    }


}
