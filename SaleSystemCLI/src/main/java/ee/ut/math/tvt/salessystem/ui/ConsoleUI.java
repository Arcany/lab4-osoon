package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.PropertiesReader;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;


/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final Warehouse warehouse;
    Properties props;
    PropertiesReader propertiesReader;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
        this.props = new Properties();
        this.propertiesReader = new PropertiesReader(props);
        this.warehouse = new Warehouse(dao);
    }

    public static void main(String[] args) throws Exception {
        log.info("javafx version: " + System.getProperty("javafx.runtime.version"));

        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();

        log.info("Salesystem CLI started");

    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }
    private void teamInfo(){
            propertiesReader.read();
            // get the property value and print it out
            System.out.println("---------------------");
            System.out.println("name: " + props.getProperty("name"));
            System.out.println("leader: " + props.getProperty("leader"));
            System.out.println("email: " + props.getProperty("email"));
            System.out.println("members: " + props.getProperty("members"));
            System.out.println("-----------------------");
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + " Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }

        System.out.println("-------------------------");
    }
    private void addStock(long id, String name, double price, int quantity){
        StockItem stockItem = new StockItem(id,name,"",price,quantity);
        log.info("adding " +stockItem + " to stock");
        log.debug("looking for matching stock. found: " + stockItem);
        warehouse.addStock(stockItem);
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + " Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private int history(){
        List<Purchase> purchases = dao.findPurchases();
        System.out.println("Enter number to see sold items");
        for (int i = 0; i < purchases.size(); i++) {
            System.out.println(i + " " + purchases.get(i));
        }
        return purchases.size();
    }
    private void soldItems(Purchase purchase){
        for (SoldItem soldItem : purchase.getSoldItems()) {
            System.out.println(soldItem);
        }
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("t\t\tShow this for team info");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("s ID NAME PRICE QTY\tAdd stock to the warehouse, ID - barcode, Name, Description, Price" +
                " And Quantity");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("h\t\tPurchase history");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) throws IOException {

        String[] c = command.split(" ");
        log.debug("processing command: " + Arrays.toString(c));
        if (c[0].equals("h")){
            int x = history();
            if (x > 0) {
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                try {
                    soldItems(dao.findPurchases().get(Integer.parseInt(in.readLine().trim().toLowerCase())));
                } catch (RuntimeException e) {
                    System.out.println("Wrong number entered");
                }
            }
        }
        else if(c[0].equals("t"))
            teamInfo();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p"))
            cart.submitCurrentPurchase();
        else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("a") && c.length == 3) {
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                if (idx < 0 || amount < 0) {
                    System.out.println("id and amount can not be negative");
                    throw new SalesSystemException("id and amount can not be negative");
                }
                StockItem item = dao.findStockItem(idx);
                if (item == null)
                    throw new SalesSystemException("no stock item with id "+ idx);
                else if (item.getQuantity() < amount)
                    throw new SalesSystemException("Amount exceeds available stock");
                else
                    cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));


            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }
        else if (c[0].equals("s") && c.length ==5) {
            try {
                long id = Long.parseLong(c[1]);
                String name = c[2];
                double price = Double.parseDouble(c[3]);
                int qty = Integer.parseInt(c[4]);
                if (price < 0 || qty < 0 || id < 0) {
                    System.out.println("id, price and amount can not be negative");
                    throw new SalesSystemException("negative value entry");
                }
                StockItem item = dao.findStockItem(id);
                if (item != null){
                    System.out.println("Item with this id already exists");
                    System.out.println("Want to add to existing stock? Y/N");
                    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                    try {
                        if (in.readLine().trim().toLowerCase().equals("y")) {
                            addStock(id, name, price, qty);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    addStock(id, name, price, qty);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }
        else {
            System.out.println("unknown command");
        }
}
}
