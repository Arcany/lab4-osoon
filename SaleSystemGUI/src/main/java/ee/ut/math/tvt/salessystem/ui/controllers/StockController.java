package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import ee.ut.math.tvt.salessystem.ui.throwErrorWindow;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private final SalesSystemDAO dao;
    private final Warehouse warehouse;

    @FXML
    private TextField barcode;
    @FXML
    private TextField amount;
    @FXML
    private TextField name;
    @FXML
    private TextField price;
    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;

    private static final Logger log = LogManager.getLogger(StockController.class);


    public StockController(SalesSystemDAO dao, Warehouse warehouse) {
        this.dao = dao;
        this.warehouse = warehouse;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        // TODO refresh view after adding new items

        // listen for barcode field changes
        this.barcode.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            log.info("found " + stockItem);
            name.setText(stockItem.getName());
            price.setText(String.valueOf(stockItem.getPrice()));
        } else {
            log.info("adding new barcode number");
            //resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        log.info("looking for existing stock");
        try {
            long code = Long.parseLong(barcode.getText());
            //if (code < 0)
            //    throw new NumberFormatException();
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            //throwErrorWindow.throwErrorWindow(new NumberFormatException("barcode must be a positive number"),log);
            return null;
        }
    }


    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    private void refreshStockItems() {
        log.info("refreshing stock items");
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
    }

    /**
     * Add new item to the warehouse.
     */
    @FXML
    public void addStockClicked() {
        log.info("Adding stock");
        long id;
        int quantity;
        double cost;
        try {
            try {
                id = Long.parseLong(barcode.getText());
                quantity = Integer.parseInt(amount.getText());
                cost = Double.parseDouble(price.getText());
            } catch (NumberFormatException e){
                throw new NumberFormatException("ID, quantity and cost must be numbers");
            }
            String n = name.getText();
            if (id < 1 || quantity < 1 || cost < 1.0) {
                throw new NumberFormatException("Numeric value fields must be above 0");
            }
            StockItem item = new StockItem(id, n, "", cost, quantity);
            warehouse.addStock(item);
        } catch (RuntimeException e) {
            throwErrorWindow.throwErrorWindow(e, log);
        }
        resetProductField();
        refreshStockItems();
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barcode.setText("");
        amount.setText("");
        name.setText("");
        price.setText("");
    }
}
