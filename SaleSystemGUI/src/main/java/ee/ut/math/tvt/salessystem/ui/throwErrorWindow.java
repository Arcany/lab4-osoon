package ee.ut.math.tvt.salessystem.ui;

import javafx.scene.control.Alert;
import org.apache.logging.log4j.Logger;

public class throwErrorWindow {
    public static void throwErrorWindow(Exception e, Logger log){
        log.error(e.toString());
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(e.getMessage());
        alert.showAndWait();
    }
}
