package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.PurchaseHistory;
import ee.ut.math.tvt.salessystem.ui.throwErrorWindow;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.event.MouseEvent;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    @FXML
    private Button showAll;
    @FXML
    private Button showLastTen;
    @FXML
    private Button showBetweenDates;
    @FXML
    private DatePicker startDateField;
    @FXML
    private DatePicker endDateField;
    @FXML
    private TableView<Purchase> purchaseHistoryTableView;
    @FXML
    private TableView<SoldItem> soldItemTableView;

    private static final Logger log = LogManager.getLogger(PurchaseController.class);
    private final SalesSystemDAO dao;
    private final PurchaseHistory purchaseHistory;


    public HistoryController(SalesSystemDAO dao, PurchaseHistory purchaseHistory) {
        this.dao = dao;
        this.purchaseHistory = purchaseHistory;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO: implement
    }

    @FXML
    protected void showBetweenDatesClicked() {

        LocalDate start = startDateField.getValue();
        LocalDate end = endDateField.getValue();
        if (start == null || end == null) {
            log.debug("could not get dates");
            throwErrorWindow.throwErrorWindow(new SalesSystemException("Choose correct dates"), log);
        }
        else {
            log.debug("got dates:" + start + "; " + end);
            fillPurchaseTable(purchaseHistory.getBetweenDates(start, end));
        }

    }

    @FXML
    protected void showLastTenClicked() {
        fillPurchaseTable(purchaseHistory.getLastTen());
    }

    @FXML
    protected void showAllClicked() {
        fillPurchaseTable(purchaseHistory.getAll());
    }
    @FXML
    protected void fillSoldItemTable() {
        Purchase purchase = purchaseHistoryTableView.getSelectionModel().getSelectedItem();
        log.debug(purchase);
        List<SoldItem> soldItems = purchase.getSoldItems();
        log.debug(soldItems);
        soldItemTableView.setItems(FXCollections.observableList(soldItems));
        soldItemTableView.refresh();
    }

    private void fillPurchaseTable(List<Purchase> purchases) {
        purchaseHistoryTableView.setItems(FXCollections.observableList(purchases));
        purchaseHistoryTableView.refresh();
    }
}
