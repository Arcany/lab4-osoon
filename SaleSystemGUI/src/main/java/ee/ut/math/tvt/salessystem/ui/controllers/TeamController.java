package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.logic.PropertiesReader;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Team tab
 * */
public class TeamController implements Initializable {
    @FXML
    private Text name;
    @FXML
    private Text leader;
    @FXML
    private Text email;
    @FXML
    private Text members;
    @FXML
    private ImageView image;

    private static final Logger log = LogManager.getLogger(TeamController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Initializing team tab");
        Properties props = new Properties();
        PropertiesReader propertiesReader = new PropertiesReader(props);
        propertiesReader.read();
        name.setText("Name: \t\t "+ props.getProperty("name"));
        leader.setText("Leader: \t\t "+props.getProperty("leader"));
        email.setText("email: \t\t "+props.getProperty("email"));
        members.setText("members: \t "+props.getProperty("members"));
        try {
            image.setImage(new Image(props.getProperty("image")));
        } catch (RuntimeException e) {
            log.error(e.toString());
        }
    }
}
