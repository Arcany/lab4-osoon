# Team Osoon:
1. Erik Marcus Maks
2. Marcus Lõo
3. Reimo Kirmann

## Homework 1:
https://bitbucket.org/Arcany/lab4-osoon/wiki/User%20Stories,%20HM1
## Homework 2:
https://bitbucket.org/Arcany/lab4-osoon/wiki/Requirements%20Specification,%20Modeling,%20Planning
## Homework 3:
https://bitbucket.org/Arcany/lab4-osoon/commits/tag/homework-3

## Homework 4:
https://bitbucket.org/Arcany/lab4-osoon/wiki/4.%20Assignment

## Homework 5:
<Links to the solution>

## Homework 6:
https://docs.google.com/spreadsheets/d/1ftkQIBolmOWOPOQDl7YIgWvpCPY-Wpxk-4hFOJ4S0hY/edit

## Homework 7:
https://bitbucket.org/Arcany/lab4-osoon/wiki/Home

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)